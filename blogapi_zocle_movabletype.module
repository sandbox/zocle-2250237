<?php

/**
 * @file
 * Provides MovableType services for BlogAPI
 */

/**
 * Implements hook_blogapi_info().
 */
function blogapi_zocle_movabletype_blogapi_info() {
  return array(
    'api_version' => 2,
    'type' => 'xmlrpc',
    'name' => 'ZocleMovableType',
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function blogapi_zocle_movabletype_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_services_resources().
 */
function blogapi_zocle_movabletype_services_resources() {
  return array(
    'zmt' => array(
      'actions' => array(
  
        'getVocabularyList' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_get_vocabularies',
          'enabled' => 1,
          'help' => 'Returns a list of all vocabularies defined in the blog.',
          'args' => array(
            array(
              'name' => 'type',
              'type' => 'string',
              'description' => 'node type',
              'source' => array('data' => 'type'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
          ),
        ),
        'getVocabularyTerms' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_get_vocabulary_terms',
          'enabled' => 1,
          'help' => 'Returns a list of all terms in a vocabulary defined in the blog.',
          'args' => array(
            array(
              'name' => 'vid',
              'type' => 'string',
              'description' => 'vocabulary ID',
              'source' => array('data' => 'vid'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
          ),
        ),
        'setPostCategories' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_set_post_categories',
          'enabled' => 1,
          'help' => 'Sets the categories for a post.',
          'args' => array(
            array(
              'name' => 'postid',
              'type' => 'int',
              'description' => 'postid',
              'source' => array('data' => 'postid'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'categories',
              'type' => 'string',
              'description' => 'categories',
              'source' => array('data' => 'categories'),
              'optional' => FALSE,
            ),
             array(
              'name' => 'vocab',
              'type' => 'int',
              'description' => 'vocabulary ID',
              'source' => array('data' => 'vocab'),
              'optional' => FALSE,
            ),
          ),
        ),
        'newMediaObject' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_new_media_object',
          'enabled' => 1,
          'help' => 'Uploads a file to your webserver.',
          'args' => array(
            array(
              'name' => 'blogid',
              'type' => 'string',
              'description' => 'blogid',
              'source' => array('data' => 'blogid'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'file',
              'type' => 'array',
              'description' => 'file',
              'source' => array('data' => 'file'),
              'optional' => FALSE,
            ),
          ),
        ),
        'setPostImage' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_edit_post',
          'enabled' => 1,
          'help' => 'Sets the Image for a post.',
          'args' => array(
            
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'file_image',
              'type' => 'array',
              'description' => 'file_image',
              'source' => array('data' => 'file_image'),
              'optional' => FALSE,
            ),
          ),
        ),
        'supportedMethods' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_supported_methods',
          'enabled' => 1,
          'help' => 'Retrieve information about the XML-RPC methods supported by the server.',
        ),
        'supportedTextFilters' => array(
          'access callback' => 'services_access_menu',
          'callback' => 'blogapi_zocle_movabletype_supported_text_filters',
          'enabled' => 1,
          'help' => 'Retrieve information about the text formatting plugins supported by the server.',
        ),
      ),
    ),
  );
}


/**
 * Service callback for zmt.setPostCategories
 * Simplified, will add new categories too
 */
function blogapi_zocle_movabletype_set_post_categories($postid, $username, $password, $categories, $vocab) {
  $node = node_load($postid);

  if (!$node) {
    return services_error(t('Node @nid not found', array('@nid' => $postid)), 404);
  }

  $not_found_errors = array();
  $wrong_vocabulary_errors = array();
  $terms_data = $node_data = array();
  
  //spit terms
  //check get id of each term
  //if not found save as new term
  if (!empty($categories)) {
    $category_array = array();
    $terms_array = explode(',', $categories);
    foreach ($terms_array as $value) {
       $tobj = taxonomy_get_term_by_name($value);
      
       if ($tobj) {
           foreach ($tobj as $tem) {
              $category_array[] = array('categoryId' => $tem->tid, 'categoryName' => $tem->name);
          }
       }
       else{
        //add term
        $new_term = new stdClass();
        $new_term->name = $value;
        $new_term->vid = $vocab;
        taxonomy_term_save($new_term);
        
        $category_array[] = array('categoryId' => $new_term->tid, 'categoryName' => $value);

       }
    }
  }
  
  $taxonomy_fields = blogapi_get_taxonomy_term_reference_fields_with_vocabularies(array($node->type));

    if (!empty($taxonomy_fields) && !empty($category_array)) {
    foreach ($category_array as $category) {
      $term = taxonomy_term_load($category['categoryId']);

      if (!$term) {
        $not_found_errors[] = $category['categoryName'];
        continue;
      }

      $success = FALSE;

      foreach ($taxonomy_fields as $field_name => $field_settings) {
        if ($field_settings['settings']['allowed_values'][0]['vocabulary'] == $term->vocabulary_machine_name) {
          $terms_data[$field_name][] = $category['categoryId'];
          $success = TRUE;
          break;
        }
      }

      if (!$success) {
        $wrong_vocabulary_errors[] = $category['categoryName'];
        continue;
      }
    }

    if (!empty($not_found_errors)) {
      return services_error(t('@terms are not found', array('@terms' => implode(' ', $not_found_errors))), 406);
    }
    elseif (!empty($wrong_vocabulary_errors)) {
      return services_error(t('@terms can\'be added to this post', array('@terms' => implode(' ', $wrong_vocabulary_errors))), 406);
    }
    else {
      foreach ($terms_data as $field_name => $terms) {

        foreach ($terms as $value) {
           $node_data[$field_name][LANGUAGE_NONE][] = array('tid' => $value);
        }
      }
      
      return blogapi_zocle_movabletype_edit_post($postid, $username, $password, array('taxonomies' => $node_data));

    }
  }

  return services_error(t('This post doesn\'t have any category'), 406);
}

/**
 * custom edit method to replace blogapi edit
 */
function blogapi_zocle_movabletype_edit_post($postid, $username, $password, $content, $publish = 1) {
  // Validate the user.
  $user = blogapi_validate_user($username, $password);

  $old_node = node_load($postid);
  $new_node = new stdClass;

  if (!$old_node) {
    return services_error(t('Node @nid not found', array('@nid' => $postid)), 404);
  }

  if (!node_access('update', $old_node, $user)) {
    return services_error(t('You do not have permission to update this post.'), 403);
  }

  // Save the original status for validation of permissions.
  $new_node->status = $publish;
  $new_node->type = $old_node->type;

  // $content can be empty sometimes, in zmt.publishPost for example
  if (!empty($content)) {
    // Let the teaser be re-generated.
    unset($old_node->teaser);

    if (is_string($content) || !empty($content['title'])) {
      $new_node->title = is_string($content) ? blogapi_blogger_extract_title($content) : $content['title'];
    }

    if (is_string($content) || !empty($content['description'])) {
      $new_node->body[LANGUAGE_NONE][0]['value'] = is_string($content) ? blogapi_blogger_extract_body($content) : $content['description'];
    }

    if (empty($content['date']) && user_access('administer nodes')) {
      $new_node->date = format_date($old_node->created, 'custom', 'Y-m-d H:i:s O');
    }

    if (!empty($content['taxonomies'])) {
      foreach ($content['taxonomies'] as $field_name => $field) {
        $old_node->{$field_name} = $field;
      }
    }

    if (!empty($content['file_image'])) {

        $file = file_load($content['file_image']);
        file_usage_add($file, 'blogapi_zocle_movabletype', 'image', $file->fid);
        $new_node->field_image[LANGUAGE_NONE][0] = (array)$file;
    }

    if (function_exists('blogapi_zocle_movabletype_blogapi_zmt_extra') && is_array($content)) {
      blogapi_zocle_movabletype_blogapi_zmt_extra($new_node, $content);
    }
  }
  return blogapi_submit_node($new_node, $old_node);
  //return $old_node;
}

/**
 * custom new media object
 */
function blogapi_zocle_movabletype_new_media_object($blogid, $username, $password, $file) {
  // Validate the user.
  $user = blogapi_validate_user($username, $password);

  $extensions = '';
  $usersize = 0;
  $uploadsize = 0;

  $roles = array_intersect(user_roles(FALSE, 'manage content with blogapi'), $user->roles);

  foreach ($roles as $rid => $name) {
    $extensions .= ' ' . strtolower(variable_get("blogapi_extensions_$rid", variable_get('blogapi_extensions_default', 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp')));
    $usersize = max($usersize, variable_get("blogapi_usersize_$rid", variable_get('blogapi_usersize_default', 1)) * 1024 * 1024);
    $uploadsize = max($uploadsize, variable_get("blogapi_uploadsize_$rid", variable_get('blogapi_uploadsize_default', 1)) * 1024 * 1024);
  }

  $filesize = strlen($file['bits']);

  if ($filesize > $uploadsize) {
    return services_error(t('It is not possible to upload the file, because it exceeded the maximum filesize of @maxsize.', array('@maxsize' => format_size($uploadsize))), 413);
  }

  if (blogapi_space_used($user->uid) + $filesize > $usersize) {
    return services_error(t('The file can not be attached to this post, because the disk quota of @quota has been reached.', array('@quota' => format_size($usersize))), 413);
  }

  // Only allow files with whitelisted extensions and convert remaining dots to
  // underscores to prevent attacks via non-terminal executable extensions with
  // files such as exploit.php.jpg.
  $whitelist = array_unique(explode(' ', trim($extensions)));

  $name = basename($file['name']);

  if ($extension_position = strrpos($name, '.')) {
    $filename = drupal_substr($name, 0, $extension_position);
    $final_extension = drupal_substr($name, $extension_position + 1);

    if (!in_array(strtolower($final_extension), $whitelist)) {
      return services_error(t('It is not possible to upload the file, because it is only possible to upload files with the following extensions: @extensions', array('@extensions' => implode(' ', $whitelist))), 403);
    }

    $filename = str_replace('.', '_', $filename);
    $filename .= '.' . $final_extension;
  }

  else {
    $filename = $name;
  }
  //$uri = file_build_uri($filename);
  $uri = ("public://zocle/image/" . $filename);
  $data = $file['bits'];

  if (!$data) {
    return services_error(t('No file sent.'), 400);
  }

  if (!$file = file_save_data($data, $uri)) {
    return services_error(t('Error storing file.'), 500);
  }

  // Store Drupal file ID in separate dfid column and unset fid to use own blogapi serial value
  $fid = $file->fid;
  $file->dfid = $file->fid;
  $fid = $file->fid;
  unset($file->fid);

  drupal_write_record('blogapi_files', $file);

  return array('fid' => $fid, 'struct');
}

/**
 * Service callback for zmt.supportedMethods
 *
 * @todo: Implement this function? system.getMethods returns the same thing,
 *        so we may not need to bother with this.
 */
function blogapi_zocle_movabletype_supported_methods() {
  return services_error(t('Not implemented'), 501);
}

/**
 * Service callback for zmt.supportedTextFilters
 */
function blogapi_zocle_movabletype_supported_text_filters() {
  // NOTE: we're only using anonymous' formats because the zmt spec
  // does not allow for per-user formats.
  $formats = filter_formats();

  $filters = array();
  foreach ($formats as $format) {
    $filter['key'] = $format->format;
    $filter['label'] = $format->name;
    $filters[] = $filter;
  }

  return $filters;
}

/**
 * Handles extra information sent by clients according to MovableType's spec.
 */
function blogapi_zocle_movabletype_blogapi_zmt_extra(&$node, $struct) {
  $was_array = FALSE;
  if (is_array($node)) {
    $was_array = TRUE;
    $node = (object)$node;
  }

  if (array_key_exists('zmt_allow_comments', $struct)) {
    switch ($struct['zmt_allow_comments']) {
      case 0:
        $node->comment = COMMENT_NODE_HIDDEN;
        break;
      case 1:
        $node->comment = COMMENT_NODE_OPEN;
        break;
      case 2:
        $node->comment = COMMENT_NODE_CLOSED;
        break;
    }
  }

  // Merge the 3 body sections (description, zmt_excerpt, zmt_text_more) into one body.
  if (isset($struct['zmt_excerpt'])) {
    $node->body[LANGUAGE_NONE][0]['value'] = $struct['zmt_excerpt'] . '<!--break-->' . $node->body[LANGUAGE_NONE][0]['value'];
  }
  if (isset($struct['zmt_text_more'])) {
    $node->body[LANGUAGE_NONE][0]['value'] = $node->body[LANGUAGE_NONE][0]['value'] . '<!--extended-->' . $struct['zmt_text_more'];
  }

  if (isset($struct['zmt_convert_breaks'])) {
    $node->body[LANGUAGE_NONE][0]['format'] = $struct['zmt_convert_breaks'];
  }

  if (isset($struct['dateCreated'])) {
    $node->date = format_date(mktime($struct['dateCreated']->hour, $struct['dateCreated']->minute, $struct['dateCreated']->second, $struct['dateCreated']->month, $struct['dateCreated']->day, $struct['dateCreated']->year), 'custom', 'Y-m-d H:i:s O');
  }

  if ($was_array) {
    $node = (array)$node;
  }
}

/**
 * Service callback for zmt.getVocabularies
 */
function blogapi_zocle_movabletype_get_vocabularies($type, $username, $password) {
  blogapi_validate_user($username, $password);
  blogapi_validate_content_type($type);

  $vocabularies = blogapi_get_vocabularies_per_content_type($type);
  if (!empty($vocabularies)) {
    foreach ($vocabularies as $vocabulary_machine_name) {
      $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);
       $vocab_array[] = array('vid' => $vocabulary->vid, 'name' => $vocabulary->name);
    }
  }
  return $vocab_array;
}

/**
 * Service callback for zmt.getVocabularyTerms
 */
function blogapi_zocle_movabletype_get_vocabulary_terms($vid, $username, $password) {
  
  blogapi_validate_user($username, $password);
    
  $terms = taxonomy_get_tree($vid);
 if (!empty($terms)) {
    foreach ($terms as $term) {
      $categories[] = array('termName' => $term->name, 'termId' => $term->tid);
    }
  }
  else {
    return services_error(t('invalid vocabulary', 406));
  }
    return $categories;
}

/**
 * hook_preprocess_page for adding custom zocle style sheets to nodes
 */
function blogapi_zocle_movabletype_preprocess_page(&$variables) {
  
  drupal_add_css('http://ne.edgecastcdn.net/00035F/scripts/zocle_20130325.css', array('group' => CSS_DEFAULT, 'type' => 'external'));
}