BlogAPI Zocle Movabletype
================================================================================================================
This is a sub module of blog api
Provides the MovableType API for BlogAPI clients.


Features
================================================================================================================

  * Supports modified operations for working with the zocle app: get all Vocabularies, set post categories, 
    set post image etc.


Documentation
================================================================================================================

Full documentation and installation instructions can be found here: http://university.zocle.com/Drupal_Integration/18804351


